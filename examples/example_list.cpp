#include "tracking_allocator.h"
#include <list>
#include <iostream>

int main(void) {

  tracking_allocator x;
  std::list<int, std::experimental::pmr::polymorphic_allocator<int>>
      container_obj(&x);

  for (int i = 0; i < 10; i++) {
    cout << i << " is about to be inserted\n";
    container_obj.push_back(i);
  }
  cout << endl << endl;
  for (int i = 0; i < 10; i++) {
    cout << 9-i << " is about to be erased\n";
    container_obj.pop_back();
  }

  cout << "\n\nEND OF PROGRAM\n";
}