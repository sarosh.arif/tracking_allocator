#include "tracking_allocator.h"
#include <iostream>
#include <unordered_map>
using namespace std;
int main(void) {

  tracking_allocator x;
  std::unordered_map<int, int, std::hash<int>, equal_to<int>,
                     std::experimental::pmr::polymorphic_allocator<int32_t>>
      container_obj(&x);

  for (int i = 0; i < 10; i++) {
    cout << i << " is about to be inserted\n";
    container_obj.insert({i, i + i});
  }
  cout << endl << endl;
  for (int i = 0; i < 10; i++) {
    cout << i << " is about to be erased\n";
    container_obj.erase(i);
  }

  cout << "\n\nEND OF PROGRAM\n";
}
