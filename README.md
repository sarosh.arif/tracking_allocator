# Tracking Allocator 
## Introduction
This program uses a tracking allocator to check the memory allocated and deallocated by the STL containers. 
### Properties
- Displays the number of bytes requested when a call for allocation is made
- Displays the number of bytes freed when a call for deallocation is made
## Compatibility
This allocator is compatible with the following containers:
- Vector
- List
- Set & unordered set
- Map & unordered map
## Example Details
Three examples are avalible: 
- vector
- list
- unordered_map

These examples are chosen because unordered map is a combination of vector and linked-list (an array of linked-list)

## Conclusion
Reserved memory is deallocated at the end of the lifecycle of container-object. 
