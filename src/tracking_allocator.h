#include <experimental/memory_resource>
#include <iostream>

using namespace std;


class tracking_allocator : public std::experimental::pmr::memory_resource
{
public:

void *do_allocate(std::size_t bytes,
                                  std::size_t alignment[[maybe_unused]])
{
    void *ptr = malloc(bytes);
    clog << "Allocating " << bytes << " bytes ";
    clog << "at address " << ptr << endl;
    return ptr;
}

void do_deallocate(void *p,
                                   std::size_t bytes[[maybe_unused]],
                                   std::size_t alignment[[maybe_unused]])
//[[maybe_unused]] Suppresses warnings on unused entities
{
    clog << "Freeing " << bytes << " bytes ";
    clog << "at address " << p << endl;
    free(p);
}

bool do_is_equal(const memory_resource &other[[maybe_unused]]) const noexcept
{
    return true;
}

};
